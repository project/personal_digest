<?php

/**
 * Implements hook_views_data_alter().
 */
function personal_digest_views_data_alter(array &$tables) {
  foreach ($tables as $tablename => &$table) {
    foreach ($table as $fieldname => &$handlers) {
      // This will break when https://www.drupal.org/node/2337507 drops.
      if (isset($handlers['argument']['field']) and $handlers['argument']['id'] === 'date_fulldate') {
        $table[$fieldname.'_since'] = [
          'title' => t('Since @fieldname', ['@fieldname' => $handlers['title']]),
          'help' => t('Needed as 1st contextual filter for personal digest emails.'),
          'description' => t('Needed as 1st contextual filter for personal digest emails.'),
          'argument' => [
            'id' => 'date_fulldate_since',
            'field' => $handlers['argument']['field']
          ]
        ];
      }
    }
  }
}

