<?php

namespace Drupal\personal_digest;

use Drupal\user\UserDataInterface;
use Drupal\Component\Datetime\Time;
use Drupal\Core\Queue\QueueInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Logger\LoggerChannel;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Config\ConfigFactory;
use Drupal\views\Entity\View;

/**
 * Provides a user password reset form.
 */
class SettingsManager {

  /**
   * @var Datetime
   */
  protected $time;

  /**
   * @var QueueInterface
   */
  protected $queue;

  /**
   * @var LoggerChannel
   */
  protected $logger;

  /**
   * @var Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * @param Time $date_time
   * @param QueueFactory $queue_factory
   * @param LoggerChannelFactory $logger
   * @param ConfigFactory $config_factory
   */
  function __construct(Time $date_time, QueueFactory $queue_factory, LoggerChannelFactory $logger, ConfigFactory $config_factory) {
    $this->time = $date_time;
    $this->queue = $queue_factory->get('personal_digest_mail');
    $this->logger = $logger->get('personal_digest');
    $this->config = $config_factory->get('personal_digest.settings');
  }

  /**
   * Get one user's settings.
   * @param int $uid
   * @return array
   */
  function forUser(int $uid) : array {
    //Retrieve the settings form the user store, with defaults.
    return (array) $this->getuserData()->get('personal_digest', $uid, 'digest')
      + ['daysoftheweek' => (array)$this->config->get('defaultdayoftheweek')]
      + $this->defaultUserSettings();
  }

  /**
   *
   * @return array
   *
   * @note that displays is empty
   */
  function defaultUserSettings() : array  {
    return [
      'displays' => $this->config->get('views'),
      'daysoftheweek' => 1,
      'weeks_interval' => 1,
      'last' => strtotime('-1 week')
     ];
  }

  /**
   * Check all user settings and dispatch mail if needed.
   *
   * This will run once every day after the set hour or once every cron,
   * whichever is less frequent.
   *
   * @return int
   *   The number of mails queued.
   *
   */
  function dispatch() : int {
    // weeks_interval value of -1 means the user wants the to get digest daily
    $daily = -1;

    $start = $this->time->getCurrentMicroTime();
    $day_of_week = date('l');
    // Find out which users to mail today
    // Check the digest settings for EVERY user.
    $all_users_data = $this->getuserData()
      ->get('personal_digest', NULL, 'digest');
    $num = 0;
    foreach ($all_users_data as $uid => $settings) {
      if (empty($settings['displays'])) {
        continue;
      }
      // Is today a sending day? or does the user want the digest every day?
      if (in_array($day_of_week, $settings['daysoftheweek'])) {
        $settings['last'] = (isset($settings['last'])) ? $settings['last'] : 0;
        // Compare the last time the cron run with the start of the current day.
        if ($settings['last'] <  strtotime('today')) {
          // Send if the specified number of weeks has passed since the last
          // mail or if the user wants to get the digest on daily basis.
          $time_since = floor(($this->time->getCurrentTime() - $settings['last']) / 604800);
          if (($settings['weeks_interval'] != $daily && $time_since > $settings['weeks_interval'] - 1) || $settings['weeks_interval'] == $daily) {
            $this->queue->createItem($uid);
            $num++;
          }
        }
      }
    }
    // Monitor performance.
    if ($num > 0) {
      $this->logger->info(
        'Checked @count user settings. Queued @num mails in @seconds seconds.',
        [
          '@count' => count($all_users_data),
          '@num' => $num,
          '@seconds' => $this->time->getCurrentMicroTime() - $start
        ]
      );
    }
    return $num;
  }

  /**
   * Put the element on the user settings form
   * @param array $form
   * @param int $uid
   * @param int $weight
   */
  function userFormElement(&$form, $uid, $weight = 10) : void {
    if ($pd_displays = $this->config->get('views')) {
      $form['personal_digest'] = [
        '#title' => 'Digest settings',
        '#type' => 'details',
        '#open' => FALSE,
        '#tree' => TRUE,
        '#weight' => $weight,
        'displays' => [
          '#type' => 'table',
          '#header' => [t('Views display'), t('Weight'), t('Enabled')],
          '#tabledrag' => [
            [
              'action' => 'order',
              'relationship' => 'sibling',
              'group' => 'weight',
            ]
          ]
        ]
      ];
      $settings = $this->foruser($uid);
      foreach ($pd_displays as $key => $display_id) {
        list($view_name, $display_name) = explode(':', $display_id);
        // Skip any views which aren't there.
        $view = View::load($view_name);
        if (!$view) {
          continue;
        }
        $display = $view->getDisplay($display_name);
        if (!$display) continue;
        $title = $view->label();// assumes only one digest display per view.
        $form['personal_digest']['displays'][$display_id] = [
          'label' => ['#markup' => ''],
          'weight' => [
            '#title' => t('Weight for @title', ['@title' => $title]),
            '#title_display' => 'invisible',
            '#type' => 'weight',
            '#default_value' => $settings['displays'][$display_id] ?? 0,
            '#attributes' => ['class' => ['weight']],
          ],
          'enabled' => [
            '#title' => $title,
            '#type' => 'checkbox',
            '#default_value' => in_array($display_id, array_keys($settings['displays'])),
            '#attributes' => ['class' => ['weight']],
          ],
          '#weight' => $settings['displays'][$display_id] ?? 0,
          '#attributes' => ['class' => ['draggable']],
        ];
      }
      uasort($form['personal_digest']['displays'], array('\Drupal\Component\Utility\SortArray', 'sortByWeightProperty'));

      $week_days = [];
      // This is a way to get the weekdays translated by php
      // Hat-tip to Carlos http://stackoverflow.com/users/462084/carlos
      // Get next Sunday.
      $day_start = date("d", strtotime("next Sunday"));
      for ($x = 0; $x < 7; $x++) {
        $unixtime = mktime(0, 0, 0, date("m"), $day_start + $x, date("y"));
        // Create weekdays array.
        $week_days[date('l', $unixtime)] = date('l', $unixtime);
      }
      $form['personal_digest']['daysoftheweek'] = [
        '#title' => t('Days of the week'),
        '#type' => 'checkboxes',
        '#options' => $week_days,
        '#default_value' => $settings['daysoftheweek'],
        '#weight' => 5,
      ];
      $form['personal_digest']['weeks_interval'] = [
        '#title' => t('Delivery frequency'),
        '#type' => 'select',
        '#options' => [
          -1 => t('Every day'),
          1 => t('Every week'),
          2 => t('Every 2 weeks'),
          4 => t('Every month'),
          8 => t('Every 2 months'),
          13 => t('Every 3 months'),
        ],
        '#default_value' => $settings['weeks_interval'],
        '#weight' => 7,
      ];
      $form['actions']['submit']['#submit'][] = [$this, 'userFormSubmit'];
    }
  }

  /**
   * Form submit handler.
   */
  function userFormSubmit($form, $form_state) {
    $displays = [];
    $submitted = $form_state->getValue('personal_digest');
    foreach ($submitted['displays'] as $display_id => $props) {
      if (!$props['enabled']) {
        continue;
      }
      $displays[$display_id] = $props['weight'];
    }

    $this->getuserData()->set(
      'personal_digest',
      $form_state->getFormObject()->getEntity()->id(),
      'digest',
      [
        'displays' => $displays,
        'daysoftheweek' => $submitted['daysoftheweek'],
        'weeks_interval' => $submitted['weeks_interval'],
      ]
    );
  }


  /**
   * if we inject user_data the user profile form can't be serialised
   */
  protected static function getuserData() : UserDataInterface {
    return \Drupal::service('user.data');
  }

}
