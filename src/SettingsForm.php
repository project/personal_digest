<?php

namespace Drupal\personal_digest;

use Drupal\user\Entity\User;
use Drupal\views\Entity\View;
use Drupal\views\Views;
use Drupal\views\ViewExecutable;
use Drupal\Core\Link;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a settings form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * @var ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * @var AccountProxyInterface
   */
  protected $currentUser;

  /**
   * @var MailManagerInterface
   */
  protected $mailManager;

  /**
   * @param ConfigFactoryInterface $config_factory
   * @param ModuleHandlerInterface $module_handler
   * @param AccountProxyInterface $current_user
   * @param MailManagerInterface $mail_manager
   */
  function __construct($config_factory, ModuleHandlerInterface $module_handler, AccountProxyInterface $current_user, MailManagerInterface $mail_manager) {
    parent::__construct($config_factory);
    $this->moduleHandler = $module_handler;
    $this->currentUser = $current_user;
    $this->mailManager = $mail_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('current_user'),
      $container->get('plugin.manager.mail')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'personal_digest_site_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $checkboxes = $options = [];
    $w = 0;
    foreach (Views::getViewsAsOptions(FALSE, 'enabled') as $key => &$view_display) {
      list($view_id, $display_id) = explode(':', $key);
      $view = View::load($view_id)->getExecutable();
      $view->setDisplay($display_id);
      if ($this->digestible($view)) {
        $options[$key] = $view_display;
        if ($this->moduleHandler->moduleExists('views_ui')) {
          $options[$key] .= ' '. Link::createFromRoute(
            t('Edit'),
            'entity.view.edit_display_form',
            ['view' => $view_id, 'display_id' => $display_id],
            ['attributes' => ['target' => 'edit digest view']]
          )->toString();
        }
      }
    }

    $settings = $this->config('personal_digest.settings');
    $hours = [];
    foreach (range(0, 23) as $number) {
      $hours[$number] = $number . ":00";
    }
    if ($options) {
      $form['views'] = [
        '#title' => $this->t('Compose the digest from these views displays'),
        '#description' => $this->t("Views displays in which the first contextual filter is '@arg'.", ['@arg' => 'date_fulldate_since']),
        '#type' => 'checkboxes',
        '#options' => $options,
        '#default_value' => $settings->get('views'),
        '#weight' => $w++
      ] + $checkboxes;

      $form['hour'] = [
        '#title' => t('Hour the digest mail will be generated'),
        '#description' => t('Mails are generated in the first cron after this hour, then a few sent every time cron runs subsequently.'),
        '#type' => 'select',
        '#options' => $hours,
        '#multiple' => FALSE,
        '#default_value' => $settings->get('hour'),
        '#weight' => $w++
      ];
      $week_days = [];
      $day_start = date("d", strtotime("next Sunday"));
      for ($x = 0; $x < 7; $x++) {
        // Create weekdays array.
        $day = date('l', mktime(0, 0, 0, date("m"), $day_start + $x, date("y")));
        $week_days[$day] = $day;
      }

      $form['defaultdaysoftheweek'] = [
        '#title' => 'Default day of the week',
        '#description' => 'Define the default day when the digest will be send',
        '#type' => 'select',
        '#options' => $week_days,
        '#default_value' => $settings->get('defaultdaysoftheweek'),
        '#weight' => $w++
      ];

      $form['info'] = [
        '#markup' => t('If the settings widget is visible on a user form display, emails will contain a link to change the settings.'),
        '#weight' => $w++
      ];
      $form['actions'] = [
        '#type' => 'actions',
        'submit' => [
          '#type' => 'submit',
          '#value' => $this->t('Submit'),
        ],
        'test' => [
          '#type' => 'submit',
          '#value' => $this->t('Submit & test'),
          '#submit' => [[$this, 'test']]
        ],
      ];
    }
    else {
      $this->messenger($this->t("There are no views yet with with a '@name' argument", ['@name' => 'date_fulldate_since']));
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('personal_digest.settings')
      ->set('views', array_values(array_filter($values['views'])))
      ->set('hour', $values['hour'])
      ->set('defaultdayoftheweek', $values['defaultdayoftheweek'])
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Submit callback
   */
  public function test(array &$form, FormStateInterface $form_state) {
    $this->submitForm($form, $form_state);
    $recipient = User::load($this->currentUser->id());
    $dummy_settings = [
      'displays' => array_flip($this->config('personal_digest.settings')->get('views'))
    ] + \Drupal::service('personal_digest.settings_manager')->defaultUserSettings();
    $sent_message = $this->mailManager->mail('personal_digest',
        'digest',
        $recipient->getEmail(),
        $recipient->getPreferredLangcode(),
        [
          'user' => $recipient,
          'since' => $dummy_settings['last'],
          'settings' => $dummy_settings
        ]
      );
    if ($sent_message['result']) {
      $this->messenger($this->t('Check your mail'));
    }
    else {
      \Drupal::logger('personal_digest')->debug('@message', ['@message' => $sent_message['subject'] ."<br />". $sent_message['body']  ]);
    }
  }
  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['personal_digest.settings'];
  }

  /**
   * Check if a views display can be used for digest.
   *
   * A view is suitable only if its first argument is type date_fulldate_since.
   *
   * @param View $view
   *   A view with the display set.
   * @return bool
   *   TRUE if the view can be used for digest.
   */
  private function digestible(ViewExecutable $view) : bool{
    if ($args = $view->getHandlers('argument')) {
      return reset($args)['plugin_id'] == 'date_fulldate_since';
    }
    return FALSE;
  }

}
