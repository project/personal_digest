<?php

namespace Drupal\personal_digest\Plugin\QueueWorker;

use Drupal\user\Entity\User;
use Drupal\user\UserDataInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Queue\Attribute\QueueWorker;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Component\Datetime\TimeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Generate the digest mail
 */
#[QueueWorker(
  id: 'personal_digest_mail',
  title: new TranslatableMarkup('Generate personalised digest mail'),
  cron: ['time' => 60]
)]
class DigestMail extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The Mail Manager service.
   *
   * @var MailManagerInterface
   */
  protected $mailManager;

  /**
   * The user data service.
   *
   * @var UserDataInterface
   */
  protected $userDataStore;

  /**
   * The time service.
   *
   * @var TimeInterface
   */
  protected $time;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param UserDataInterface $user_data
   *   UserData service.
   * @param MailManagerInterface $mail_manager
   *   Mail manager service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   */
  public function __construct($configuration, $plugin_id, $plugin_definition, UserDataInterface $user_data, MailManagerInterface $mail_manager, TimeInterface $time) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->userDataStore = $user_data;
    $this->mailManager = $mail_manager;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('user.data'),
      $container->get('plugin.manager.mail'),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($uid) {
    $settings = $this->getSettings($uid);
    if (empty($settings['displays'])) {
      return;
    }
    // Send from the latest of the 'last interval' and 'last'
    if ($settings['weeks_interval'] > 0) {
      // Send every x weeks
      $since = strtotime('-' . $settings['weeks_interval'] . ' weeks');
    }
    else {
      // Daily digests
      $since = strtotime('-24 hours');
    }
    $since = min(intval($settings['last']), $since);

    // Prevent sending too frequently if the queue got blocked.
    if ($settings['last'] > $since) {
      return;
    }

    $recipient = User::load($uid);
    $this->mailManager
      ->mail(
        'personal_digest',
        'digest',
        $recipient->getEmail(),
        $recipient->getPreferredLangcode(),
        [
          'user' => $recipient,
          'since' => $since,
          'settings' => $settings
        ]
      );
    // The mail is sent, so save the last sent time.
    $settings['last'] = $this->time->getRequestTime();
    $this->userDataStore->set('personal_digest', $uid, 'digest', $settings);
  }

  /**
   * Get the user's digest settings
   * @param int $uid
   * @return array
   */
  private function getSettings(int $uid) : array {
    $settings = (array)$this->userDataStore->get('personal_digest', $uid, 'digest');
    $settings += [
      'weeks_interval' => 1,
      'last' => 0,
      'displays' => []
    ];
    //$settings['displays'] = array_filter($settings['displays']);
    return $settings;
  }

}
