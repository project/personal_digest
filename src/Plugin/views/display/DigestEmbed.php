<?php

namespace Drupal\personal_digest\Plugin\views\display;
use Drupal\views\Plugin\views\display\Embed;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\Attribute\ViewsDisplay;

/**
 * The plugin that handles an digest display, requires that there be a date argument.
 *
 * @ingroup views_display_plugins
 */
#[ViewsDisplay(
  id: 'digest_embed',
  title: new TranslatableMarkup('Digest'),
  help: new TranslatableMarkup('A display which is available to comprise personal digests.'),
  theme: 'views_view',
  uses_menu_links: FALSE
)]
class DigestEmbed extends Embed {

  /**
   * {@inheritDoc}
   *
   * This display only works with the date as a first argument
   */
  public function validate() {
    $errors = parent::validate();
    $args = $this->getHandlers('argument');
    $first_arg = reset($args);
    if ($first_arg->getPluginId() <> 'date_fulldate_since') {
      $errors['digest'] = "The first argument on the digest display must be a 'Since' argument.";
    }
    return $errors;
  }

}
